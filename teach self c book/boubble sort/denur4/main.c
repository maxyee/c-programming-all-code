#include <stdio.h>
#include <stdlib.h>

int main()
{
   int n, sum = 0, c;

   printf("sample input : \n");
   scanf("%d", &n);



   for (c = 1; c <= n; c++)
   {
      sum=sum+(c*c)*(c+1);
   }

   printf("Sample output = %d\n",sum);

   return 0;
}

/*
void main()
 {
     int i,n,sum=0;
     scanf("%d",&n);
     for(i=1;i<=n;i++) {

        sum=sum+(i*i)*(i+1);
     }
     printf("result is %d",sum);

 }
*/
